# git'ting it

A talk/discussion introducing git. Focused on helping network engineers properly function in a git repository.

[//]:#(###############################################################################################################)

## Table of Contents

- [git'ting it](#gitting-it)
  - [Table of Contents](#table-of-contents)
  - [Abstract](#abstract)
    - [Versioning the Old Way](#versioning-the-old-way)
  - [Why version control](#why-version-control)
  - [Installing git](#installing-git)
  - [git init](#git-init)
  - [git status](#git-status)
  - [git add](#git-add)
  - [git add .](#git-add)
  - [git rm](#git-rm)
  - [Empty files vs. empty directories](#empty-files-vs-empty-directories)
  - [Using the .gitignore file](#using-the-gitignore-file)
    - [Example #1: Ignore a file](#example-1-ignore-a-file)
    - [Example #2: Ignore via wildcard](#example-2-ignore-via-wildcard)
    - [Tracking a file that matches .gitinore criteria](#tracking-a-file-that-matches-gitinore-criteria)
  - [git commit](#git-commit)
    - [Authorship](#authorship)
    - [Perform the commit already!](#perform-the-commit-already)
    - [Thoughts on contents of commits](#thoughts-on-contents-of-commits)
    - [Thoughts on commit messages](#thoughts-on-commit-messages)
  - [git log](#git-log)
  - [Gitlab and GitHub](#gitlab-and-github)
  - [git clone](#git-clone)
    - [Example](#example)
  - [git fetch](#git-fetch)
    - [Example with no differences between local/remote repositories](#example-with-no-differences-between-localremote-repositories)
    - [Example with differences between local/remote repositories](#example-with-differences-between-localremote-repositories)
  - [git pull](#git-pull)
    - [Sum up](#sum-up)
  - [git push](#git-push)
  - [git diff](#git-diff)
  - [git branch(es)](#git-branches)
  - [git merge](#git-merge)

[//]:#(###############################################################################################################)

## Abstract

Network engineers are increasingly needing to step up their programmer game.  Along with learning Python, Bash, YAML,
Markdown, and several other languages and file formats they also REALLY need to understand version controlling their
project files.  The current forerunner in the version control world is **git**.  This session will start at the very
beginning and walk through using git tools that are most commonly used.

### Versioning the Old Way

Do you create a new dated folder for each new "version" of your files?

![versioning the old way](images/versioning_the_old_way.png)

Or maybe, do you add a "date" to the new file?

![versioning the old way v2](images/versioning_the_old_way-v2.png)

**Then this talk is for you!**

[//]:#(###############################################################################################################)

## Why version control

Version control is a mechanism of keeping a record of a series of changes within a group of digital assets.  Typically
text based resources, such as Python scripts and/or Markdown documentation, work best because changes can be recorded on
a line-by-line basis.  This series of checkpoints give you a historical timeline which gives you the ability to revert
to previous checkpoints of your tracked digital assets.  So, for example, should you accidentally delete a file you
shouldn't have, or cut/pasted instead of copy/pasting but didn't notice until later you can recover by reverting to
a previous checkpoint prior to this mistake.

There are many version control systems available but **git** is the industry's current favorite.  With git you not only
get the ability to go forward/backward in your digital asset's timeline you also have the ability to branch off and
then re-merge this branch, so that you can test something without messing up the main timeline.  If that wasn't enough
of a reason to use git, it also provides a great way to co-develop your digital asset with others as well as an easy way
to share your digital asset with anyone.

[//]:#(###############################################################################################################)

## Installing git

Installing git is fairly easy no matter which OS platform you choose to use.  That said, this talk uses Ubuntu 18.10
Desktop as the OS so the screenshots and command outputs will be formatted based on this choice.

That said, installing git on any Linux-base distribution is easiest of all the OSes... this is because most Linux
distributions have some sort of package management system.  For example, on Debian/Ubuntu based systems simply typing
`sudo apt install git` will do the trick.  Other Linux distributions will have a similar syntax (but use `yum` or `yast`
 or some other package manager instead of `apt`.)

For Windows and MacOS there are various options.  The only one I've had experience with is
[git-scm.com](https://git-scm.com/downloads).

Though this talk is an introduction to git, it will assume that you have already installed git on your machine.

[//]:#(###############################################################################################################)

## git init

If you are starting a brand new repository then use the command `git init` in the root of the directory containing
the digital assets you wish to track in this particular project.  The whole suite of git commands will then become
available for use against any files or folders you place in this directory.

The `git init` command will create a new folder in the current directory named **.git**.  Note:  Because of the
leading dot this folder will be hidden by default.

![git init](images/git_init.png)

This is where **ALL** the versioning information about this particular repository will be stored.  This is key to
understand as this means that you can move/rename the top-level folder and the git information will not care.  Or, if
you want to stop using git for the files in this folder you can delete the .git folder and this folder is now clean of
git.  (This also means you've removed all the versioning history of changes to files/folders in this root directory
though too.  Consider very carefully whether you really want to remove the .git folder before doing so.)

Directly after running `git init` the .git folder is essentially a bare bones structure.  Most of the time you won't
need to concern yourself with the contents of the .git directory BUT it is somewhat good to know where it is and that
all your versioned files are stored in it.

![tree of git dir](images/tree_git_dir.png)

Note:  You can `git init` a directory that already has existing files.  All this will do is build a bare bones .git
directory structure for you.  All the files/folders within the current directory will be flagged as "untracked" and can
easily be added to the git repository.  (More on doing this later.)

[//]:#(###############################################################################################################)

## git status

Assuming you initialized git in an empty directory the command `git status` will display fairly bland information.
That said, this information, bare as it is, has some key pieces of information.

![git status of empty dir](images/git_status_empty_dir.png)

The first bit of information shows you that you are on the "master" branch.  Branches will be discussed
[later](#git-branches) but it is important to know that your first branch, after initializing git in a directory, is
called **master**.

The second section will list out which files need to be committed, if any.  As this is a fresh initialization there
isn't anything in the staging area (more on staging in just a minute) to commit.

The last section describes any files that are currently untracked or have changed since they were last added to git for
tracking.

So, for example, whether you create a new file or files existed in the directory prior to using `git init` they will be
listed in the Untracked files section.

![git status of untracked file](images/git_status_untracked_file.png)

[//]:#(###############################################################################################################)

## git add

Many people don't fully understand what the command `git add` really does.  First and most importantly:
`git add` does **NOT** add files to the git repository!!!  `git add` adds files to an area within git called
staging.  Files that are in the staging area are the ones that will get added to the git repository once a commit
occurs.

From our previous section, we created a file named test_file.txt.  The command `git status` indicated that there is an
untracked file since we have yet to add the test_file.txt file into git.  Issuing the command `git add test_file.txt`
will indicate to git that you wish to start tracking the status of the file named test_file.txt.

![add test file](images/add_test_file.png)

Notice that `git status` now indicates that we have a file in git that has yet to be committed.  This means that
test_file.txt is now in the staging area.  Though we have yet to commit this file, git has already added it into the
.git directory.

![tree showing git object](images/tree_showing_git_object.png)

[//]:#(###############################################################################################################)

## git add .

Many people learn to issue the command `git add .`.  This command tells git to add to the staging area all files that
are currently not tracked and/or have changes.  I'm **STRONGLY** recommending that you **DO NOT** use the command
`git add .`.  Why do I say this?  Well, once a file has been committed to git is is hard, if not impossible, to remove
it from the repository.  Blanketly adding all files that have changes or not added to the git repository ends up adding
files that have passwords in them, or adding the whole Python virtualenv directory, etc.  So, until you are **very**
comfortable with git do not use the `git add .` command syntax.

[//]:#(###############################################################################################################)

## git rm

In the previous section we talked about how to add files to the git content management system and that once a file is
"added" it is in there effectively forever.  That said, you do have a way to "untrack" files using the
`git rm 'filename'` command.

For example, even if we wanted to "remove" the test_file.txt file from git, that object will remain in the .git's
object directory.

![remove staged file](images/remove_staged_file.png)

[//]:#(###############################################################################################################)

## Empty files vs. empty directories

As we have seen, git will notice and allow you to add an empty file (a file with a size of 0 bytes).  However, git will
not see nor interact with empty folders.  If your project requires the existence of an "empty" folder (for example,
an output directory for configurations you build in your Python script) then you'll either need to deal with that logic
in your program or you can pre-create the directory and add a "placeholder" file in that directory.  Add that
"placeholder" file into your git repository and the directory will also be added.

![placeholder file](images/placeholder_file.png)

[//]:#(###############################################################################################################)

## Using the .gitignore file

In the last section we learned about adding files to git.  I urged caution about adding files that probably shouldn't be
tracked (like files with passwords in them, or a Python virtual environment directory).  Remembering to **not** add
these files every time you need to add files into git can be tedious.  However, git has a solution to this problem!  
Create a file named **.gitignore** in the root directory of your project.

![make gitignore file](images/make_gitignore_file.png)

This file is a list of files, folders, or wildcard matches of files/folders that should always be marked as
"do not track" in this git repository.

### Example #1: Ignore a file

Suppose we want a file named secrets.txt in our project but we don't want the contents to be added to the
git repository.  Edit the .gitignore file and add a line with secrets.txt on it.

![ignored secrets](images/ignored_secrets.png)

Now create the secrets.txt file and add your super secret password into that file.  Git will not show that file as
"untracked" in the `git status` output even though that file is present in the project's directory.

![not tracking secrets](images/not_tracking_secrets.png)

**Notice that git respects the .gitignore file even if the .gitignore file isn't being tracked by git.**

### Example #2: Ignore via wildcard

Listing each file that git should ignore can be tedious.  The .gitignore file supports wildcards too.  In this example
we want to ignore all files under a folder called "env".

![wildcard ignore](images/wildcard_ignore.png)

A good way to use the .gitignore file is to issue the command `git status` and view the list of untracked files.
Any files listed that you wish to NEVER add to your repository should get added to the .gitignore file.

### Tracking a file that matches .gitinore criteria

Let's assume, by default, you wish to ignore all files ending int ".txt".  Add a line in the .gitignore file for *.txt.
BUT then you want to add a specific file that ends in .txt.  Issue the command `git add -f <name>.txt` to force the
tracking of this specific file and thus overriding the .gitignore exception.

Using Example #2, let's assume we wanted to ignore all files in the "env" directory **except** the file2.txt file.  Use
the command `git add -f env/file2.txt` to force add it to be tracked by git.

![force add](images/force_add.png)

[//]:#(###############################################################################################################)

## git commit

The command `git commit` takes a few variables to make a hash (which will be the commit ID of this commit) to uniquely
identify this specific moment in this git repository.  These variables include: the author's information, the previous
commit's hash value, the commit message, as well as a few other bits of information.

**This is key:**
Probably the most important variable used to create this hash is the previous commits' hash.  This means that each
commit is "bound" the the previous commits.  There is **no way** to slip a commit in-between two commits.  The Internet
is full of ways to get around this problem (mostly by rolling back to an older commit and then building a whole new
commit tree) but for the beginner/intermediate user of git the key concept to take away from this is that your commits
are tied to the commits that came before it.

Note:  It is too complex to explain at this moment but it should be noted that a commit can have multiple (previous)
commits it is tied to.  This happens when merging branches.  More about this later.

**More key thoughts:**
For all practical purposes (aka for non-advanced git users) this is the last chance you have to NOT add a file or
changes to a file to the permanent records within git.  After using the command `git commit` any files in the staging
area will be moved from the staging area into the repository... forever.  Even if you delete these files and then
re-commit those files exist in the archives.  This is why using `git add .` is dangerous for a beginner (and even an
intermediate) user of git.

### Authorship

Depending on whether you have set up your author's information globally (or in this local repositories config) you might
need to tell git who the author is for this repository.

On this Ubuntu server I have yet to configure that information

![no authorship](images/no_authorship.png)

Use the commands `git config user.email` and `git config user.name` to configure this git repository with your
authorship information.

![set authorship](images/set_authorship.png)

This information will then be added to the config file in the .git folder.

![cat git config](images/cat_git_config.png)

### Perform the commit already!

Now with the requisite information loaded we can commit our staged files (or changes to files) with the
`git commit -m "<insert commit message here>"` command.

![initial commit](images/inital_commit.png)

Note: If you choose not to put the commit message in at the command line you can issue the command `git commit` and a
text editor (probably nano) will open up for you to type in a message.

### Thoughts on contents of commits

Technically, just like in troubleshooting, a commit should only cover **1** change.  (Not 1 line of change but 1 topic
or problem.)  In my experience this **rarely** happens.  If you want to be exacting, to meet this recommendation then be
selective with your `git add` commands to only add files to a particular commit that go together.  (Then `git add` the
next batch for the next commit.)

### Thoughts on commit messages

What to say in a commit message can be very challenging.  It is recommended that the message not be past tense, nor
overly wordy.  What helps me in making my commit message it to think of the message like it is a magical spell's
incantation.  Pretend that if the commit message was to be said aloud that it would "magically" morph your project into
a state that matches this commit's state.

For example, presume you found a bug in your project and you fixed it via the commit for which you are trying to write
a message.  The commit message could be something like: "Resolve bug that turns the screen blue" or "Fix blue screen".

[//]:#(###############################################################################################################)

## git log

Now that we've seen how to add files to git and how to commit our changes it is time to see how to review the "history"
of what has been done.  The key command to do this is `git log`.

![blah](images/blah.png)

As you can see, the log show each commit, who is the author, the date it was committed, as well as the commit message.
If you haven't done any branching (more about branches later) then this output is probably sufficient.  However, a
"prettier" version of this output can be seen adding some flags.  Try `git log --graph --decorate`

![blah](images/blah.png)

The "--graph and --decorate" options help see how the branches split (and possibly merge) into each other.  At this stage
in your git learning this is a bit too advanced but I wanted you to know how to see your commit history at a minimum.

[//]:#(###############################################################################################################)

## Gitlab and GitHub

So far all the commands covered have been only interacting and affecting the files in the git repository in the local
directory.  Chances are sharing this project's files and all the git history with others is desired.  You could just zip
up the contents of this folder and distribute that zip with whomever.  However, git can handle this using what is
called a "remote repository".

This remote repository can be synchronized with all the state of this git's project via the git
"[push](./push.md)/[pull](./fetch_and_pull.md)" commands. (More about those commands in a bit.)
The location of this remote repository needs to be accessible to whomever needs access to this project.  Though there
are many services out on the Internet to host remote git repositories the two most popular are
[GitLab](https://gitlab.com) and [GitHub](https://github.com).

Both GitLab and GitHub have a vast array of features you can use.  Anything from simple hosting of your project to
integrating into CI/CD workflows.  Not all these features are free and I'm not here to exhaustively discuss said
features.  Both services do, however, give you a "cloud" based location to store your project which can then be shared
out to others for collaborative reasons or just plainly a way to spread the use of your project.

[//]:#(###############################################################################################################)

## git clone

If you are starting to work on a project that already exists on a remote repository (such as hosted at GitLab or GitHub)
then the `git clone <URL>` command will grab a copy of that project and extract it into a local folder on your machine.

This clone will grab all the files in the repository as well as the .git directory.  This should give you a complete
replica of this repository located on your local machine.

### Example

To clone this project to a local directory, issue the command `git clone https://gitlab.com/daxm/git-ing_it.git`

![git clone](images/git_clone.png)

This creates a folder named the name of the remote repository ("git-ing_it" in this case).

Within this directory is the whole git repository that was stored at the remote repository.

![cloned tree](images/cloned_tree.png)

Note:  The command `tree -a` would have also shown the contents of hidden directories (like ".git") but the output was
too long to take a screenshot.

Though [git branches](./branch.md) has yet to be discussed it should be noted that the *master* branch is the initially
checked out branch BUT any branch on the remote repository can be checked out locally.

![show branches](images/show_branches.png)

The key message here is that the cloning process essentially copies the whole repository from the remote and creates a
local repository.

[//]:#(###############################################################################################################)

## git fetch

Let's assume that you've cloned a repository and then some time has passed.  You are unsure whether someone else on your
team has made changes to the remote repository (from which you grabbed this project).  The command `git fetch` will
compare your local git repository to the remote repository and look for changes.  Then use the command `git status` to
see the results.

### Example *with no differences* between local/remote repositories

The *master* branch of this project is in sync between my local and remote repositories.  (I know this ahead of time but
wanted to explain for this example's purposes.)

Issuing `git fetch` checks that the local repository's last commit matches the remote.  (Essentially ensuring that the
local and remote branches of this repository are at the same point.)
![git fetch in sync](images/git_fetch_in_sync.png)

### Example *with differences* between local/remote repositories

I'm working in the "dev" branch of this project.  So, first, I need to change to the "dev" branch.  Once there, I issue
the command `git fetch`.  This time the local repository's latest commit is NOT the same as the remote's.

![git fetch not in sync](images/git_fetch_not_in_sync.png)

![git status not in sync](images/git_status_not_in_sync.png)

[//]:#(###############################################################################################################)

## git pull

If there are changes on the remote repository that you don't have locally you can then use the `git pull` to update your
local repository with those changes.

![git pull changes](images/git_pull_changes.png)

If everything is in sync you'll know.

![git pull no changes](images/git_pull_no_changes.png)

This command is one of the first ones that trip up a beginner user of git.  Not that the command's use is hard but more
that beginners end up in a situation where they have changed a file locally that has also been changed on the remote
repository.  This causes something called a collision and collision resolution is something that beginners should
definitely avoid as much as possible.  For now, the easy way to avoid collisions it to use git's [branch](./branch.md)
feature.  More about that shortly.

### Sum up

To recap: `git fetch` can be used to compare the local vs. remote repositories to see if they are in sync (or not).
`git pull` is used to download from the remote repository any commits not present on the local copy of the repository.
(Those differences can be seen using the `git diff` command.  More about that command later.)

[//]:#(###############################################################################################################)

## git push

The command `git push` is essentially the reverse action of `git pull`.  It compares the remote repository's
commits and uploads any that are missing from the local repository.

[//]:#(###############################################################################################################)

## git diff

stuff goes here

[//]:#(###############################################################################################################)

## git branch(es)

The use of branches (and merging of branches) in git will be the most advanced feature that we will cover today.
Branches are a way of "isolating" changes to your project in such a way that won't affect the "mainline" commit history.
As the name implies a branch separates off a new commit line from the master branch to create a whole new commit line.
A branch does "originate" from some 'master branch' commit so the two can be eventually re-combined, if necessary.
Use the command `git branch blah` to create a new branch named 'blah' (using the current latest commit as the 'seed'
of this branch) and the use the command `git checkout blah` to switch to using this (or another) branch.  These two
actions can be combined with the command `git checkout -b foo` to both create and switch to the 'foo' branch.

Use the command `git branch` to not only see all the available branches but to also see which is currently checked
out (the one with the * next to it).

[//]:#(###############################################################################################################)

## git merge

Though it isn't a requirement for branches to be merged together, it is common practice to "bring together" branches.
Typically this is after the branch's use has been completed and tested.  Whatever changes that were made in the branch
can then be re-integrated into the 'master' branch.  (Though you can easily go the other way and merge any changes from
the 'master' branch into the currently checked out branch.  In fact you can merge 'branch-a' into 'branch-b' if you so
desire.)

Just prior to merging branches is the most critical (and advanced) moment we will cover today. 'BLAH'
